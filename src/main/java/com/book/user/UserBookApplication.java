package com.book.user;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.book.user.mapper")
public class UserBookApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserBookApplication.class, args);
    }

}
