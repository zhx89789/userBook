package com.book.user.controller;

import com.book.user.domain.Promotion;
import com.book.user.service.PromotionService;
import com.book.user.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @author: zhx
 * @date: 2023/5/15
 */
@RestController
public class PromotionController {

    @Autowired
    private PromotionService promotionService;

    //测试地址URL：http://localhost:8080/fenxiang?userId=1002&bookId=200

    @GetMapping("/fenxiang")
    public AjaxResult getFenxiangle(Integer userId, Integer bookId) {
        Promotion promotion = promotionService.getPromotion(userId, bookId);
        return AjaxResult.success(promotion);
    }
}
