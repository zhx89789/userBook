package com.book.user.mapper;

import com.book.user.domain.Promotion;
import org.springframework.stereotype.Repository;

/**
 * @Description:
 * @author: zhx
 * @date: 2023/5/15
 */
@Repository
public interface PromotionMapper {

    //新增推广数据
    int insertPromotion(Promotion promotion);

    //新增推广码
    int updatePromotion(Promotion promotion);

    //查询推广码数据
    Promotion selectPromotion(Promotion promotion);
}
