package com.book.user.service;

import com.book.user.domain.Promotion;

/**
 * @Description:
 * @author: zhx
 * @date: 2023/5/15
 */

public interface PromotionService {

    Promotion getPromotion(Integer userId, Integer bookId);
}
