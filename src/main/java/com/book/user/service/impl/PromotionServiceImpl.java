package com.book.user.service.impl;

import com.book.user.domain.Promotion;
import com.book.user.mapper.PromotionMapper;
import com.book.user.service.PromotionService;
import com.book.user.util.DateUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @Description:
 * @author: zhx
 * @date: 2023/5/15
 */
@Service
public class PromotionServiceImpl implements PromotionService {

    @Autowired
    private PromotionMapper promotionMapper;

    @Override
    public Promotion getPromotion(Integer userId, Integer bookId) {
        //1、根据用户ID和书籍ID查询推广码
        Promotion query = new Promotion();
        query.setUserId(userId);
        query.setBookId(bookId);
        Promotion promotion = promotionMapper.selectPromotion(query);
        //2、推广码查询为空则生成一个并且插入到表中
        if (promotion == null) {
            Promotion insert = new Promotion();
            insert.setUserId(userId);
            insert.setBookId(bookId);
            String code = RandomStringUtils.randomAlphanumeric(12);//生成12位推广码
            insert.setPromotionCode(code);
            insert.setPromotionTime(new Date());
            insert.setCreateTime(new Date());
            //插入表中
            int i = promotionMapper.insertPromotion(insert);
            if (i == 1) {
                //3、插入成功则返回
                return insert;
            }
        } else {
            //4、如果可以查询到，判断推广码创建时间是否大于30天
            long day = DateUtils.getDatePoorDay(new Date(), promotion.getPromotionTime());
            if (day > 30) {
                //重新生成一个推广码，并且更新到表中
                String code = RandomStringUtils.randomAlphanumeric(12);//生成12位推广码
                promotion.setPromotionCode(code);
                promotion.setPromotionTime(new Date());
                //更新推广码并且返回
                promotionMapper.updatePromotion(promotion);
                return promotion;
            } else {
                return promotion;
            }
        }
        return null;
    }
}
